var mongoose = require('mongoose');
//MongoDB object modeling tool - interact with database

var bcrypt = require('bcryptjs');
//password hashing function

// User Schema
var UserSchema = mongoose.Schema({
	username: {
		type: String,
		index:true
	},
	password: {
		type: String
	},
	email: {
		type: String
	},
	name: {
		type: String
	}
});

var User = module.exports = mongoose.model('User', UserSchema);
//passing the variables to outside


module.exports.createUser = function(newUser, callback)
//
{
	bcrypt.genSalt(10, function(err, salt)
 	//salt - generating random bytes to hash the pw
	{
	    bcrypt.hash(newUser.password, salt, function(err, hash) {
	        newUser.password = hash;
	        newUser.save(callback);
	    });
	});
}

module.exports.getUserByUsername = function(username, callback){
	var query = {username: username};
	User.findOne(query, callback);
}


module.exports.comparePassword = function(passwordTypedin, hash, callback){
	bcrypt.compare(passwordTypedin, hash, function(err, isMatch) {
    	if(err) throw err;
    	callback(null, isMatch);
	});
}

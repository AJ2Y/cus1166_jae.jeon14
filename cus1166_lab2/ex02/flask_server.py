# This code demostrates a simple api_server.
from flask import Flask
app = Flask(__name__)

@app.route('/readHello')
def getRequestHello():
    return "Hello, You sent a GET Request!"

## This is a route, the response to a POST request to the URL /createHello
@app.route('/createHello', methods = ['POST'])
def postRequestHello():
    return "Hello, you sent a POST message"

## This is a route, the response to a PUT request to the URL /updateHello
@app.route('/updateHello', methods = ['PUT'])
def updateRequestHello():
    return "Hello, you sent a PUT request!"

## This is a route, the response to a DELETE request to the URL /deleteHello
@app.route('/deleteHello', methods = ['DELETE'])
def deleteRequestHello():
    return "Hello, you sent a received a DELETE request!"


if __name__ == '__main__':
    app.run()

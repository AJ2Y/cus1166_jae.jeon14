import requests
import json
import unittest

class TestAPI(unittest.TestCase):

    # This test the GET request to readHello endpoint.
    def test_getrequest(self):
        print()
        print("Executing 'GET request' Test ")
        print("-----------------------------")
        url = "http://127.0.0.1:5000/readHello"
        r = requests.get(url)
        print("Response: " + r.text)
        # Asset if response is right
        self.assertIn("GET",r.text)

    # This test the POST request to createHello endpoint.
    def test_postrequest(self):
        print()
        print("Executing 'POST request' Test ")
        print("-----------------------------")
        url = "http://127.0.0.1:5000/createHello"
        r = requests.post(url)
        print("Response: " + r.text)
        # Asset if response is right
        self.assertIn("POST",r.text)

    # This test the PUT request to updateHello endpoint.
    def test_putrequest(self):
        print()
        print("Executing 'PUT request' Test ")
        print("-----------------------------")
        url = "http://127.0.0.1:5000/updateHello"
        r = requests.put(url)
        print("Response: " + r.text)
        # Asset if response is right
        self.assertIn("PUT",r.text)

    # This test DELETE request to /deleteHello endpoint
    def test_deleterequest(self):
        print()
        print("Executing 'DELETE request' Test ")
        print("-------------------------------")
        url = "http://127.0.0.1:5000/deleteHello"
        r = requests.delete(url)
        print("Response: " + r.text)
        # Asset if response is right
        self.assertIn("DELETE",r.text)

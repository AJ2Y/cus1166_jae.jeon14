import requests
import json
import unittest
import random


#
#  Test sending get request.
#

class TestAPI(unittest.TestCase):

    def test_postrequest(self):
        print()
        print("Executing 'POST request' Test ")
        print("-----------------------------")

        url = "http://127.0.0.1:5000/books"
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain' }
        data = {
             "id" : random.randint(1,101),
             "title" : "Introduction to Something",
        }
        r = requests.post(url,data=json.dumps(data), headers=headers)
        print("Response: " + r.text)
        # Asset if response is right
        self.assertIn("OK",r.text)

    def test_getrequest(self):
        print()
        print("Executing 'GET request' Test ")
        print("-----------------------------")
        url = "http://127.0.0.1:5000/books"
        r = requests.get(url)
        print("Response: " + r.text)
        # Asset if response is right
        self.assertIn("OK",r.text)


    def test_getrequest_details(self):
        print()
        print("Executing 'GET request' READ - Test ")
        print("-----------------------------")
        url = "http://127.0.0.1:5000/books/100"
        r = requests.get(url)
        print("Response: " + r.text)
        # Asset if response is right
        self.assertIn("OK",r.text)

    def test_putrequest(self):
        print()
        print("Executing 'PUT request' Test ")
        print("-----------------------------")
        url = "http://127.0.0.1:5000/books/100"
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain' }
        data = {
             "id" : random.randint(1,101),
             "title" : "Introduction to Something",
        }
        r = requests.put(url,data=json.dumps(data), headers=headers)
        print("Response: " + r.text)
        # Asset if response is right
        self.assertIn("OK",r.text)

    def test_deleterequest(self):
        print()
        print("Executing 'DELETE request' Test ")
        print("-------------------------------")
        url = "http://127.0.0.1:5000/studies/100"
        r = requests.delete(url)
        print("Response: " + r.text)
        # Asset if response is right
        self.assertIn("OK",r.text)

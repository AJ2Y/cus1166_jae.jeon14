from flask import Flask, request
import json

response_dict = {
        "status" : "OK",
        "data" : {"bookid": 100,
                   "title" : "Introduction to something"
                 }
    }
json.dumps(response_dict)


app = Flask(__name__)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)

@app.route('/books', methods=['GET','POST'])
def all_books_handler():
    if request.method == 'GET':
        # Call the method to Get all of the studies
        return getListOfBooks()

    elif request.method == 'POST':
           #Call the method to make a new study
        return createNewBook()

@app.route('/books/<int:bookid>',methods=['GET','PUT','DELETE'])
def book_handler(bookid):
    if request.method == 'GET':
        return getBookDetails(bookid)

    elif request.method == 'PUT':
        book_details = None
        return updateBookDetais(bookid,book_detais)

    elif request.method == 'DELETE':
        return  removeBook(bookid)
def getListOfBooks():
    return "GET - This method will return a list of books."

def createNewBook():
    return "POST - Created new book"

def getBookDetails(bookid):
    return "GET - This method will return the details with of a book with id " % studyid

def updateBookDetais(bookid):
    return "PUT - Study with id %s is updated" % bookid

def removeBook(bookid):
    return "DELETE - Study with id %s is removed" % bookid

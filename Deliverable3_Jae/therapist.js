var mongoose = require('mongoose');
//MongoDB object modeling tool - interact with database

var bcrypt = require('bcryptjs');
//password hashing function

// User Schema
var User2Schema = mongoose.Schema({
	username: {
		type: String,
		index:true
	},
	password: {
		type: String
	},
	email: {
		type: String
	},
	name: {
		type: String
	}
});

var User2 = module.exports = mongoose.model('User2', User2Schema);
//passing the variables to outside


module.exports.createUser = function(newUser, callback)
//
{
	bcrypt.genSalt(10, function(err, salt)
	// 10: default- the # of rounds the data is processed for
	// higher == secure hashed data == slower process
 	//salt - generating random bytes to hash the pw
	{
	    bcrypt.hash(newUser.password, salt, function(err, hash) {
	        newUser.password = hash;
	        newUser.save(callback);
	    });
	});
}

module.exports.getUserByUsername = function(username, callback){
	var query = {username: username};
	User2.findOne(query, callback);
}

module.exports.getUserById = function(id, callback){
	User2.findById(id, callback);
}

module.exports.comparePassword = function(passwordTypedin, hash, callback){
	bcrypt.compare(passwordTypedin, hash, function(err, isMatch) {
    	if(err) throw err;
    	callback(null, isMatch);
	});
}

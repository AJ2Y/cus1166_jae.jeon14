var User = require('../models/user');

var users = new User();
users.load(function(err) {
  if (err) {
    console.log('Error: ' + err);
    users.close();
    return;
  }
  users.getUserList(function(err, userList) {
    if (err) {
      console.log('Error: ' + err);
      users.close();
      return;
    }
    if (userList.length) {
      console.log('users:');
      for (var i = 0, len = userList.length; i < len; i++) {
        console.log('  ' + (i + 1) + '. ' + userList[i]);
      }
    } else {
      console.log('no patients');
    }
    users.close();
  });
});

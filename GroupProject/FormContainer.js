﻿
import React, {Component} from 'react';  

import CheckBox from '../components/CheckBox';  
import Input from '../components/Input';   
import Select from '../components/Select';
import Button from '../components/Button'

class FormContainer extends Component {  
  constructor(props) {
    super(props);

    this.state = {
      newUser: 
      {
        name: '',
        age: '',
        gender: '',
        interventionTime: '',

      },

      genderOptions: ['Male', 'Female', 'Others']

    }
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleClearForm = this.handleClearForm.bind(this);
  }

handleFormSubmit() {
  // Form submission logic
}
handleClearForm() {
  // Logic for resetting the form
}

  render() {
    return (
      <form className="container" onSubmit={this.handleFormSubmit}>

        <Input /> {/* Name of the user */}
        <Input /> {/* Input for Age */} 
        <Select /> {/* Gender Selection */}
        <CheckBox /> {/* Available time slot for intervention*/}
        <Button /> { /*Submit */ }
        <Button /> {/* Clear the form */}
      </form>
    );
  }
}

export default FormContainer;
}

const Input = (props) => {
    return (  
  <div className="form-group">
    <label htmlFor={props.name} className="form-label">{props.title}</label>
    <input
      className="form-input"
      id={props.name}
      name={props.name}
      type={props.type}
      value={props.value}
      onChange={props.handleChange}
      placeholder={props.placeholder} 
    />
  </div>
)
}

export default Input;

<Input type={'text'}
               title= {'Full Name'} 
               name= {'name'}
               value={this.state.newUser.name} 
               placeholder = {'Enter your name'}
               handleChange = {this.handleFullName}
               /> {/* Name of the user */}
  
handleFullName(e) {
  let value = e.target.value;
  this.setState( prevState => ({ newUser : 
      {...prevState.newUser, name: value
      }
    }))
  }
(prevState, props) => stateChange
this.handleFullName = this.handleFullName.bind(this)

handleAge(e) {
       let value = e.target.value;
   this.setState( prevState => ({ newUser : 
        {...prevState.newUser, age: value
        }
      }), () => console.log(this.state.newUser))
  }

handleInput(e) {
     let value = e.target.value;
     let name = e.target.name;
     this.setState( prevState => {
        return { 
           newUser : {
                    ...prevState.newUser, [name]: value
                   }
        }
     }, () => console.log(this.state.newUser)
     )
 }

const Select = (props) => {
    return(
        <div className="form-group">
            <label htmlFor={props.name}> {props.title} </label>
            <select
              name={props.name}
              value={props.value}
              onChange={props.handleChange}
              >
              <option value="" disabled>{props.placeholder}</option>
              {props.options.map(option => {
                return (
                  <option
                    key={option}
                    value={option}
                    label={option}>{option}
                  </option>
                );
              })}
            </select>
      </div>)
}

export default Select;

<Select title={'Gender'}
       name={'gender'}
       options = {this.state.genderOptions} 
       value = {this.state.newUser.gender}
       placeholder = {'Select Gender'}
       handleChange = {this.handleInput}
/> {/* Gender Selection */}



const CheckBox = (props) => {

    return( <div>
    <label for={props.name} className="form-label">{props.title}</label>
    <div className="checkbox-group">
      {props.options.map(option => {
        return (
          <label key={option}>
            <input
              className="form-checkbox"
              id = {props.name}
              name={props.name}
              onChange={props.handleChange}
              value={option}
              checked={ props.selectedOptions.indexOf(option) > -1 }
              type="checkbox" /> {option}
          </label>
        );
      })}
    </div>
  </div>
);
}

interventionCheckBox(e) {

    const newSelection = e.target.value;
    let newSelectionArray;

    if(this.state.newUser.interventionTime.indexOf(newSelection) > -1) {
      newSelectionArray = this.state.newUser.interventionTime.filter(s => s !== newSelection)
    } else {
      newSelectionArray = [...this.state.newUser.interventionTime, newSelection];
    }

      this.setState( prevState => ({ newUser:
        {...prevState.newUser, interventionTime: newSelectionArray }
      })
      )
}

const Button = (props) => {
    console.log(props.style);
    return(
        <button 
            style= {props.style} 
            onClick= {props.action}>    
            {props.title} 
        </button>)
}

export default Button;

handleClearForm(e) {

      e.preventDefault();
      this.setState({ 
        newUser: {
          name: '',
          age: '',
          gender: '',
          interventionTime: []
        },
      })
  }

handleFormSubmit(e) {
    e.preventDefault();
    let userData = this.state.newUser;

    fetch('http://example.com',{
        method: "POST",
        body: JSON.stringify(userData),
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
      }).then(response => {
        response.json().then(data =>{
          console.log("Successful" + data);
        })
    })
  }   
